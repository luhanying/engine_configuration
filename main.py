from flask import(Flask, request, session, redirect, url_for, render_template, flash)
from config import(load_config, set_config)
from helperfunctions import (validate_ip, restart_management_service)
app = Flask(__name__)

# Load default config
app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='engine key',
    USERNAME='configure',
    PASSWORD='enginePa55word'
))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username!'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password!'
        else:
            session['logged_in'] = True
            return redirect(url_for('configurations'))
    return render_template('login.html', error=error)


@app.route('/')
def main():
    return redirect(url_for('login'))


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out!')
    return redirect(url_for('login'))


@app.route('/configurations', methods=['GET', 'POST'])
def configurations():
    if not session.get('logged_in'):
        redirect(url_for('login'))
    error = None
    success = None
    result = {}
    try:
        if request.method == 'POST':
            if not validate_ip(request.form["server_ip"]):
                error = "Server IP is not valid"
            elif not validate_ip(request.form["som1_ip"]):
                error = "SoM 1 IP is not valid"
            elif not validate_ip(request.form["som2_ip"]):
                error = "SoM 2 IP is not valid"
            if error is None:
                result, error = set_config(request.form)
            if error is None:
                error = restart_management_service()
            if error is None:
                success = 'Engine configurations updated! Restarting management service...'
            if success is None:
                result, _ = load_config()
            return render_template('configurations.html', result=result, error=error, success=success)

        result, error = load_config()
        return render_template('configurations.html', result=result, error=error)

    except Exception:
        return render_template('configurations.html', result=result, error="Internal server error!")


if __name__ == '__main__':
    app.run(debug=app.config['DEBUG'], host='0.0.0.0', port=5006, threaded=True)
