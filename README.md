# engine_configuration

This web app allows user to plug an Ethernet cable and configure engine through GUI.

####Activate the virtual environment:

- On OS X and Linux, do the following:
`
$ . venv/bin/activate
`

- If you are using Pycharm, go to File--Settings--Project Interpreter, use the venv folder as the interpreter.


- If you want to go back to the real world, use the following command: 
`$ deactivate`


####Run the app:

- In virtual environment:

`python main.py`

See app configuration in main.py for login credentials.

####Access GUI on browser:

http://0.0.0.0:5006