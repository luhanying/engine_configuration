"""Helper functions."""
import sys

from subprocess import call


def validate_ip(addr):
    """Validate ip address."""
    try:
        a = addr.split('.')
        if len(a) != 4:
            return False
        for x in a:
            if not x.isdigit():
                return False
            i = int(x)
            if i < 0 or i > 255:
                return False
        return True
    except:
        return False


def execute_command(cmd):
    """Execute a system call."""
    out = sys.stdout
    return call(cmd, stdout=out, stderr=out)


def get_service_status():
    """Get the status of the service."""
    cmd = ["systemctl", "status", "management.service"]
    return execute_command(cmd) == 0


def restart_service():
    """Restart the service."""
    cmd = ["systemctl", "restart", "management.service"]
    return execute_command(cmd) == 0


def restart_management_service():
    """Restart management service and check the status."""
    if not restart_service() or not get_service_status():
        return "Restarting management service failed!"
    return None
