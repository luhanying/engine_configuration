""""This moduel reads and/or modifies /opt/skycope/data/management_config.yaml."""
import traceback
import yaml


_CONFIG_PATH = "/opt/skycope/data/management_config.yaml"


def load_config():
    """
    Load default values from config file.
    :return: a dictionary contains result object and error message.
    """
    try:
        with open(_CONFIG_PATH, "rt") as f:
            config = yaml.safe_load(f.read())
            result = {}
            result["server_hostname"] = config["server_info"]["name"]
            result["server_ip"] = config["server_info"]["ip"]
            result["som1_ip"] = config['sensor_boards']['board1_info']['ip']
            result["som2_ip"] = config['sensor_boards']['board2_info']['ip']
            result["internal_if"] = config['sensor_box']['internal_if']
            result["external_if"] = config['sensor_box']['external_if']
            return result, None
    except Exception:
        print(traceback.format_exc())
        return {}, "Default values could not be loaded"


def set_config(new_values):
    """Set values into config file.
    :param new_values: a dictionary containing new values.
    :return: a dictionary containing new values. Upon unsuccessful write old values and error message will be returned
    """
    try:
        with open(_CONFIG_PATH, "rt") as f:
            config = yaml.safe_load(f.read())

        for key, value in new_values.items():
            config["server_info"]["name"] = new_values["server_hostname"]
            config["server_info"]["ip"] = new_values["server_ip"]
            config['sensor_boards']['board1_info']['ip'] = new_values["som1_ip"]
            config['sensor_boards']['board2_info']['ip'] = new_values["som2_ip"]
            config['sensor_box']['internal_if'] = new_values["internal_if"]
            config['sensor_box']['external_if'] = new_values["external_if"]

        with open(_CONFIG_PATH, 'w') as f:
            yaml.safe_dump(config, f)

        return new_values, None

    except Exception:
        print(traceback.format_exc())
        return {}, "New values could not be set in config file"
